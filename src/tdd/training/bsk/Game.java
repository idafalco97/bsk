package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	protected ArrayList<Frame> frames; 
	protected int firstBonusThrow;
	protected int secondBonusThrow;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frames = new ArrayList<>(10);
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(this.frames.size()>=10)
			throw new BowlingException("you can't add another frame");
		this.frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
			if(index >= 10 || index<0)
				throw new BowlingException("Invalid index");
			return frames.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}
	/**
	 * It sets the bonus for the strike.
	 */
	public void calculateBonusStrike(Frame f, int index)throws BowlingException{
		if(frames.indexOf(f)<8) {
			
			if(this.getFrameAt(index+1).isStrike()) {
				f.setBonus(this.getFrameAt(index+1).getFirstThrow() + this.getFrameAt(index+2).getFirstThrow());
			}
			else {
				f.setBonus(this.getFrameAt(index+1).getFirstThrow() + this.getFrameAt(index+1).getSecondThrow());
			}
		}
		
		else if(frames.indexOf(f)==8 && this.getFrameAt(index+1).isStrike()) {
			
			f.setBonus(this.getFrameAt(index+1).getFirstThrow() + this.getFirstBonusThrow());
		}
		
		else {
			f.setBonus(this.getFirstBonusThrow()+this.getSecondBonusThrow());
		}
	}
	

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(Frame f : frames) {
			int index = frames.indexOf(f);
			if(f.isSpare()) {
				if(frames.indexOf(f)<9) {
					
					f.setBonus(this.getFrameAt(index+1).getFirstThrow());
				}
				else {
					f.setBonus(this.getFirstBonusThrow());
				}
			}
			if(f.isStrike()) {
				
				calculateBonusStrike(f, index);
				
			}
				
			score = score + f.getScore();
		}
		return score;
	}

}
